
package SchoolSystem;

import java.util.ArrayList;

public abstract class User {
    
    private String forename, surname, username, password;
    private int ID;
    
    public User(String forename, String surname, String username, 
            String password) {
        this.forename = forename;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }
    
    public User(String forename, String surname, String username, 
            String password, int ID) {
        this(forename, surname, username, password);
        this.ID = ID;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getID() {
        return ID;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    @Override
    public String toString() {
        return forename + " " + surname;
    }
}
