
package SchoolSystem;

import java.util.ArrayList;


public class Student extends User {
    
    private ArrayList<StudentSubject> studentSubjects;
    
    public Student(String forename, String surname, String username, 
            String password) {
        super(forename, surname, username, password);
        studentSubjects = new ArrayList<>();
    }

    public ArrayList<StudentSubject> getSubjects() {
        return studentSubjects;
    }

    public void setSubjects(ArrayList<StudentSubject> studentSubjects) {
        this.studentSubjects = studentSubjects;
    }
     
    
    @Override
    public String toString() {
        return super.toString();
    }
}
