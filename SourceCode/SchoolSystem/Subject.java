
package SchoolSystem;


public class Subject {
    
    private String name;
    private int ID;
    
    public Subject(String name) {
        this.name = name;

    }
      
    public Subject(int ID, String name) {
        this(name);
        this.ID = ID;
    }
    
    public int getID() { 
        return ID;
    }
    
    public String getName() {
        return name;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
