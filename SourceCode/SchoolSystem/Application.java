
package SchoolSystem;

import GUI.*;
import java.util.ArrayList;

public class Application {
    
    public static void main(String[] args) { 
        DatabaseController dbcontroller = new DatabaseController();
        //connection
        try {
            dbcontroller.setConnection();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

        new LoginFrame(dbcontroller);
    }
}
