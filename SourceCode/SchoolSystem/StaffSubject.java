
package SchoolSystem;

import java.util.ArrayList;


public class StaffSubject extends Subject {
    
    private ArrayList<Student> students;
    
    
    public StaffSubject(int ID, String name) {
        super(ID,name);
        students = new ArrayList<>();
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }
}
