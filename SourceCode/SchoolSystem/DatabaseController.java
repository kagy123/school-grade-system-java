
package SchoolSystem;

import java.sql.*;
import java.util.ArrayList;

public class DatabaseController {
    
    private String dbaseUrl;
    private String username;
    private String password;
    private Connection con;
    private static Statement stmt;
    private static ResultSet rs;
    private static String sqlStr;
    private static int colCount;
    
    public DatabaseController() {
        dbaseUrl = "jdbc:oracle:thin:@crusstuora1.staffs.ac.uk:1521:stora";
        username = "K029263E";
        password = "k029263e";
        con = null;
        
    }
    
    public Connection setConnection() throws Exception {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
            con = DriverManager.getConnection(dbaseUrl, username, password);
        }
        catch (Exception e){
            String msg = "Cannot establish a connection to the database.";
            throw new Exception(msg);
        }
        finally {
            return con;
        }
    }
    
    public ArrayList<StudentSubject> loadStudentSubjects(String username) throws Exception {
        ArrayList<StudentSubject> subjects = new ArrayList<>();
        
        sqlStr = "SELECT subject.ID, subject.\"NAME\", \n" +
                    "student_subject.first_grade,\n" +
                    "student_subject.second_grade,\n" +
                    "student_subject.third_grade\n" +
                    "FROM subject\n" +
                    "INNER JOIN student_subject\n" +
                    "ON subject.ID = student_subject.SUBJECT_ID\n" +
                    "WHERE STUDENT_SUBJECT.STUDENT_USERNAME = '" + username + "'";
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlStr);
        }
        catch (Exception e){
            String msg = "problem with executing query";
            throw new Exception(msg);
        }
        
        //process the result set
        try {
            colCount = rs.getMetaData().getColumnCount();
            while(rs.next()) {
                StudentSubject sub = null;
                for(int i = 1; i <= colCount; i++) {
                    sub = new StudentSubject(Integer.parseInt(rs.getString("ID")), 
                            rs.getString("name"));
                    if(rs.getString("first_grade") != null) {
                        sub.setFirstTermGrade(Double.parseDouble(rs.getString("first_grade")));
                    }
                    if(rs.getString("second_grade") != null) {
                        sub.setSecondTermGrade(Double.parseDouble(rs.getString("second_grade")));
                    }
                    if(rs.getString("third_grade") != null) {
                        sub.setThirdTermGrade(Double.parseDouble(rs.getString("third_grade")));
                    }
                }
                subjects.add(sub);
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e) {
            String msg = "problem with process the result set";
            throw new Exception(msg);
        }
        return subjects;
    }
    
    public Student getStudentLogin(String username, String password) throws Exception {
        Student s = null;
        sqlStr = "select * from student\n" +
                "where student.USERNAME = '" + username + "'" 
                + " AND student.PASSWORD = '" + password + "'";
        
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlStr);
        }
        catch (Exception e){
            String msg = "problem with executing query";
            throw new Exception(msg);
        }
        
        //process the result set
        try {
            colCount = rs.getMetaData().getColumnCount();
            while(rs.next()) {
                for(int i = 1; i <= colCount; i++) {
                    s = new Student(rs.getString("forename"),
                    rs.getString("surname"), rs.getString("username"),
                    rs.getString("password"));
                }
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e) {
            String msg = "problem with process the result set";
            throw new Exception(msg);
        }
        return s;
    }
    
    public Staff getStaffLogin(String username, String password) throws Exception {
        Staff s = null;
        sqlStr = "select * from staff\n" +
                "where staff.USERNAME = '" + username + "'" 
                + " AND staff.PASSWORD = '" + password + "'";
        
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlStr);
        }
        catch (Exception e){
            String msg = "problem with executing query";
            throw new Exception(msg);
        }
        
        //process the result set
        try {
            colCount = rs.getMetaData().getColumnCount();
            while(rs.next()) {
                for(int i = 1; i <= colCount; i++) {
                    s = new Staff(rs.getString("forename"),
                    rs.getString("surname"), rs.getString("username"),
                    rs.getString("password"));
                }
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e) {
            String msg = "problem with process the result set";
            throw new Exception(msg);
        }
        return s;
    }
    
    public ArrayList<StaffSubject> loadStaffSubjects(String username) throws Exception {
        ArrayList<StaffSubject> subjects = new ArrayList<>();
        
        sqlStr = "SELECT subject.ID, subject.\"NAME\"\n" +
                "FROM subject\n" +
                "INNER JOIN staff_subject\n" +
                "ON subject.ID = staff_subject.SUBJECT_ID\n" +
                "WHERE staff_subject.STAFF_USERNAME = '" + username + "'";
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlStr);
        }
        catch (Exception e){
            String msg = "problem with executing query";
            throw new Exception(msg);
        }
        
        //process the result set
        try {
            colCount = rs.getMetaData().getColumnCount();
            while(rs.next()) {
                StaffSubject sub = null;
                for(int i = 1; i <= colCount; i++) {
                    sub = new StaffSubject(Integer.parseInt(rs.getString("ID")), 
                            rs.getString("name"));
                }
                subjects.add(sub);
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e) {
            String msg = "problem with process the result set";
            throw new Exception(msg);
        }
        return subjects;
    }
    
    public ArrayList<Student> loadSubjectStudends(int subjectID) throws Exception {
        ArrayList<Student> students = new ArrayList<>();
        
        sqlStr = "SELECT student.USERNAME, student.PASSWORD, student.FORENAME, student.SURNAME\n" +
                "FROM student\n" +
                "INNER JOIN student_subject\n" +
                "ON student_subject.SUBJECT_ID = " + subjectID + "\n" +
                "WHERE student_subject.STUDENT_USERNAME = student.USERNAME";
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlStr);
        }
        catch (Exception e){
            String msg = "problem with executing query";
            throw new Exception(msg);
        }
        
        //process the result set
        try {
            colCount = rs.getMetaData().getColumnCount();
            while(rs.next()) {
                Student s = null;
                for(int i = 1; i <= colCount; i++) {
                    s = new Student(rs.getString("forename"),
                    rs.getString("surname"), rs.getString("username"),
                    rs.getString("password"));
                }
                students.add(s);
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e) {
            String msg = "problem with process the result set";
            throw new Exception(msg);
        }
        return students;
    }
    
    public StudentSubject getStudentSubject(String username, int subjectID, String subjectName) throws Exception {
        StudentSubject s = null;
        sqlStr = "SELECT student_subject.STUDENT_USERNAME, student_subject.SUBJECT_ID,\n" +
                "student_subject.FIRST_GRADE, student_subject.SECOND_GRADE,\n" +
                "student_subject.THIRD_GRADE\n" +
                "FROM student_subject\n" +
                "WHERE student_subject.SUBJECT_ID = " + subjectID + 
                " and student_subject.STUDENT_USERNAME = '" + username + "'";
        
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sqlStr);
        }
        catch (Exception e){
            String msg = "problem with executing query";
            throw new Exception(msg);
        }
        
        //process the result set
        try {
            colCount = rs.getMetaData().getColumnCount();
            while(rs.next()) {
                for(int i = 1; i <= colCount; i++) {
                    s = new StudentSubject(Integer.parseInt(rs.getString("subject_id")),
                    subjectName);
                    if(rs.getString("first_grade") != null) {
                        s.setFirstTermGrade(Double.parseDouble(rs.getString("first_grade")));
                    }
                    if(rs.getString("second_grade") != null) {
                        s.setSecondTermGrade(Double.parseDouble(rs.getString("second_grade")));
                    }
                    if(rs.getString("third_grade") != null) {
                        s.setThirdTermGrade(Double.parseDouble(rs.getString("third_grade")));
                    }
                }
            }
            rs.close();
            stmt.close();
        }
        catch (Exception e) {
            String msg = "problem with process the result set";
            throw new Exception(msg);
        }
        return s;
    }
    
    public boolean updateGrade(int subjectID, String username, int term, double grade) throws Exception {
        switch(term) {
            case 1:
                sqlStr = "UPDATE student_subject" + "\n" +
                        "SET first_grade = " + grade + "\n" +
                        "WHERE student_username = '" + username + "' AND subject_id = " + subjectID;
                break;
            case 2:
                sqlStr = "UPDATE student_subject" + "\n" +
                        "SET second_grade = " + grade + "\n" +
                        "WHERE student_username = '" + username + "' AND subject_id = " + subjectID;
                break;
            case 3:
                sqlStr = "UPDATE student_subject" + "\n" +
                        "SET third_grade = " + grade + "\n" +
                        "WHERE student_username = '" + username + "' AND subject_id = " + subjectID;
                break;
            default:
                return false;
        }
        try {
            stmt = con.createStatement();
            int rowCount = stmt.executeUpdate(sqlStr);
            return true;
        }
        catch (Exception e){
            String msg = "problem with executing query";
            throw new Exception(msg);
        }
    }
}
