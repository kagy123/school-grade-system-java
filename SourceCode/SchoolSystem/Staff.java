
package SchoolSystem;

import java.util.ArrayList;


public class Staff extends User {
    
    private ArrayList<StaffSubject> subjects;
    
    public Staff(String forename, String surname, String username,
            String password) {
        super(forename, surname, username, password);
        subjects = new ArrayList<>();
    }

    public ArrayList<StaffSubject> getStaffSubjects() {
        return subjects;
    }

    public void setStaffSubjects(ArrayList<StaffSubject> subjects) {
        this.subjects = subjects;
    }
    
    @Override
    public String toString() {
        return super.toString();
    }
}
