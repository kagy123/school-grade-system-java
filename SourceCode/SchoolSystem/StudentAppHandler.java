
package SchoolSystem;

import java.text.DecimalFormat;

public class StudentAppHandler {
    
    private Student student;
    
    public StudentAppHandler(Student student) {
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    
    public String getFirstTermGrades() {
        String grades = "First term:\n";
        for(StudentSubject s : student.getSubjects()) {
            grades += "      " + s.getName() + " -> " ;
            if(s.getFirstTermGrade()!= -1) {
                grades += s.getFirstTermGrade()+ "\n";
            }
            else {
                grades += "N/A\n";
            }
        }
        return grades;
    }
    
    public String getSecondTermGrades() {
        String grades = "Second term:\n";
        for(StudentSubject s : student.getSubjects()) {
            grades += "      " + s.getName() + " -> " ;
            if(s.getSecondTermGrade()!= -1) {
                grades += s.getSecondTermGrade()+ "\n";
            }
            else {
                grades += "N/A\n";
            }
        }
        return grades;
    }
    
    public String getThirdTermGrades() {
        String grades = "Third term:\n";
        for(StudentSubject s : student.getSubjects()) {
            grades += "      " + s.getName() + " -> " ;
            if(s.getThirdTermGrade()!= -1) {
                grades += s.getThirdTermGrade()+ "\n";
            }
            else {
                grades += "N/A\n";
            }
        }
        return grades;
    }
    
    public String getOverallGrades() {
        DecimalFormat df = new DecimalFormat("#.00");
        String grades = "Overall grades:\n";
        for(StudentSubject s : student.getSubjects()) {
            grades += "      " + s.getName() + " -> " ;
            if(s.getOverallGrade() != -1) {
                grades += df.format(s.getOverallGrade()) + "\n";
            }
            else {
                grades += "N/A\n";
            }
        }
        return grades;
    }
    
    public String getAllStudentGrades() {
        DecimalFormat df = new DecimalFormat("#.00");
        String grades = "First term:\n";
        for(StudentSubject s : student.getSubjects()) {
            grades += "      " + s.getName() + " -> " ;
            if(s.getFirstTermGrade()!= -1) {
                grades += s.getFirstTermGrade()+ "\n";
            }
            else {
                grades += "N/A\n";
            }
        }
        grades += "Second term:\n";
        for(StudentSubject s : student.getSubjects()) {
            grades += "      " + s.getName() + " -> " ;
            if(s.getSecondTermGrade()!= -1) {
                grades += s.getSecondTermGrade()+ "\n";
            }
            else {
                grades += "N/A\n";
            }
        }
        grades += "Third term:\n";
        for(StudentSubject s : student.getSubjects()) {
            grades += "      " + s.getName() + " -> " ;
            if(s.getThirdTermGrade()!= -1) {
                grades += s.getThirdTermGrade()+ "\n";
            }
            else {
                grades += "N/A\n";
            }
        }
        grades += "Overall grades:\n";
        for(StudentSubject s : student.getSubjects()) {
            grades += "      " + s.getName() + " -> " ;
            if(s.getOverallGrade() != -1) {
                grades += df.format(s.getOverallGrade()) + "\n";
            }
            else {
                grades += "N/A\n";
            }
        }
        return grades;
    }
    
    public String getStudentSubjects() {
        String subjects = "";
        for(StudentSubject s : student.getSubjects()) {
            subjects += s.getName() + "\n";
        }
        return subjects;
    }
}
