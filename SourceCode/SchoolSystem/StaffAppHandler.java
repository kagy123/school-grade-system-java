
package SchoolSystem;


public class StaffAppHandler {
    
    private Staff staff;
    
    public StaffAppHandler(Staff staff) {
        this.staff = staff;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }
}

