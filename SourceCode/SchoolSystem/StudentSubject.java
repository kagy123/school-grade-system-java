
package SchoolSystem;

import java.text.DecimalFormat;


public class StudentSubject extends Subject {
    
    private double firstTermGrade;
    private double secondTermGrade;
    private double thirdTermGrade;
    
    public StudentSubject(int student_subject_id, String name) {
        super(student_subject_id, name);
        firstTermGrade = -1;
        secondTermGrade = -1;
        thirdTermGrade = -1;
    }

    public double getOverallGrade() {
        DecimalFormat df = new DecimalFormat("#.00");
        double grade = 0.00;
        if(firstTermGrade != -1) {
            grade += ((firstTermGrade*33)/100);
        }
        if(secondTermGrade != -1) {
            grade += ((secondTermGrade*33)/100);
        }
        if(thirdTermGrade != -1) {
            grade += ((thirdTermGrade*34)/100);
        }
        String gradeString = df.format(grade);
        return Double.parseDouble(gradeString);
    }

    public double getFirstTermGrade() {
        return firstTermGrade;
    }

    public double getSecondTermGrade() {
        return secondTermGrade;
    }

    public double getThirdTermGrade() {
        return thirdTermGrade;
    }

    public void setFirstTermGrade(double firstTermGrade) {
        this.firstTermGrade = firstTermGrade;
    }

    public void setSecondTermGrade(double secondTermGrade) {
        this.secondTermGrade = secondTermGrade;
    }

    public void setThirdTermGrade(double thirdTermGrade) {
        this.thirdTermGrade = thirdTermGrade;
    }
    
    public String toStringGrades() {
        String s = "First term -> ";
        if(firstTermGrade != -1) {
            s += firstTermGrade;
        }
        else {
            s += "N/A";
        }
        
        s += ", Second term -> ";
        if(secondTermGrade != -1) {
            s += secondTermGrade;
        }
        else {
            s += "N/A";
        }
        
        s += ", Third term -> ";
        if(thirdTermGrade != -1) {
            s += thirdTermGrade;
        }
        else {
            s += "N/A";
        }
        
        s += ", Overall -> " + getOverallGrade();
        
        return s;
    }
    
    @Override
    public String toString() {
        return super.getName() + ": " + toStringGrades();
    }
}
