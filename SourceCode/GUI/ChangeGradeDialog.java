
package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;

public class ChangeGradeDialog extends JDialog {
    private String[] numbersAfter, numbersBefore, terms;
    private String prompt;
    private JButton OK, cancel;
    private JLabel promptLabel, termLabel, dotLabel, gradeLabel;
    private JComboBox termCombo, beforeCombo, afterCombo;
    private JPanel southFormPanel, northFormPanel, centerFormPanel;
    private boolean cancelled;
    private double grade;
    private int term;
    
    public ChangeGradeDialog(Frame owner, String title, String prompt) {
        super(owner, title, true);   
        this.prompt = prompt;
        cancelled = true;
        setupArrays();
        setSize(300,150);
        setLayout(new BorderLayout());
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setupGUI();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    public void setGrade(Double grade) {
        this.grade = grade;
    }
    
    public Double getGrade() {
        return grade;
    }
    
    public void setTerm(int term) {
        this.term = term;
    }
    
    public int getTerm() {
        return term;
    }
    
    private void setupGUI() {
        setupTextComponents();
        setupButtons();
        setupComboBoxes();
        setupJPanels();
        addActionListteners();
        getContentPane().setBackground(Color.decode("#FFFF99"));
    }
    
    private void setupButtons() {
        OK = new JButton("OK");
        cancel = new JButton("Cancel");
    }     
    
    private void setupTextComponents() {
        promptLabel = new JLabel(prompt);
        termLabel = new JLabel("Term:");
        gradeLabel = new JLabel("Grade:");
        dotLabel = new JLabel(".");
    }
    
    private void setupComboBoxes() {
        termCombo = new JComboBox(terms);
        beforeCombo = new JComboBox(numbersBefore);
        afterCombo = new JComboBox(numbersAfter);
    }
    
    private void setupJPanels() {
        northFormPanel = new JPanel();
        northFormPanel.setBackground(Color.decode("#FFFF99"));
        southFormPanel = new JPanel();
        southFormPanel.setBackground(Color.decode("#FFFF99"));
        centerFormPanel = new JPanel();
        centerFormPanel.setBackground(Color.decode("#FFFF99"));
        
        northFormPanel.add(promptLabel);
        
        JPanel termPanel = new JPanel();
        termPanel.setBackground(Color.decode("#FFFF99"));
        termPanel.add(termLabel);
        termPanel.add(termCombo);
        JPanel gradePanel = new JPanel();
        gradePanel.setBackground(Color.decode("#FFFF99"));
        gradePanel.add(gradeLabel);
        gradePanel.add(beforeCombo);
        gradePanel.add(dotLabel);
        gradePanel.add(afterCombo);
        centerFormPanel.add(termPanel);
        centerFormPanel.add(gradePanel);
        
        southFormPanel.add(OK);
        southFormPanel.add(cancel);
        
        getContentPane().add(southFormPanel, BorderLayout.SOUTH);
        getContentPane().add(centerFormPanel, BorderLayout.CENTER);
        getContentPane().add(northFormPanel, BorderLayout.NORTH);
    }
    

    
    private void setupArrays() {
        numbersBefore = new String[101];
        for(int i = 0; i <= 100; i++) {
            numbersBefore[i] = Integer.toString(i);
        }
        numbersAfter = new String[100];
        for(int i = 0; i < 100; i++) {
            numbersAfter[i] = Integer.toString(i);
        }
        terms = new String[3];
        for(int i = 0; i < 3; i++) {
            terms[i] = Integer.toString(i+1);
        }
    }
    
    private void addActionListteners() {
        beforeCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(beforeCombo.getSelectedIndex() == 100) {
                    afterCombo.setEnabled(false);
                }
                else {
                    afterCombo.setEnabled(true);
                }
            } 
        });
        
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelled = false;
                dispatchEvent(new WindowEvent(getOwner(), WindowEvent.WINDOW_CLOSING));
            }
        });
        
        OK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int choice = JOptionPane.showConfirmDialog(rootPane, 
                       "Are you sure you want to change the grade?",
                       "Grade Change Confirmation", JOptionPane.YES_NO_OPTION, 
                       JOptionPane.QUESTION_MESSAGE);
                if(choice == 0) {
                    if(afterCombo.isEnabled()) {
                        grade = Double.parseDouble((String)beforeCombo.getSelectedItem() 
                            + "." + (String)afterCombo.getSelectedItem());
                    }
                    else {
                        grade = Double.parseDouble((String)beforeCombo.getSelectedItem()); 
                    }
                    setTerm(Integer.parseInt((String)termCombo.getSelectedItem()));
                    setGrade(grade);
                    dispatchEvent(new WindowEvent(getOwner(), WindowEvent.WINDOW_CLOSING));
                }
            }
        });
        
        this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                cancelled = false;
            }
        });
    }
    public boolean wasCancelled() {
        return cancelled;
    }
}
