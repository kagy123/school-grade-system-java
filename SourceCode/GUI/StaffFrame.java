
package GUI;

import SchoolSystem.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class StaffFrame extends JFrame{
    
    private DatabaseController dbcontroller;
    private StaffAppHandler sApphndl;
    private JLabel sNameLabel, subjectListLabel;
    private JPanel centerFormPanel, northFormPanel, southFormPanel;
    private JList studentList;
    private JScrollPane listScroller;
    private JComboBox subjectList;
    private DefaultListModel<Student> model;
    private JButton changeGradesButton, viewGradesButton, logoutButton;
    private ChangeGradeDialog changeGradeDialog;
    
    public StaffFrame(Staff staff, DatabaseController dbcontroller) {
        super("School System - Staff Menu");
        this.dbcontroller = dbcontroller;
        sApphndl = new StaffAppHandler(staff);
        getContentPane().setLayout(new BorderLayout());
        getContentPane().setBackground(Color.decode("#FFFF99"));
        setupButtons();
        setupTextComponents();
        setupLists();
        setupJPanels();
        addActionListeners();
        this.setSize(440, 350);
        setLocationRelativeTo(null);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void setupButtons() {
        viewGradesButton = new JButton("View Grades");
        viewGradesButton.setEnabled(false);
        changeGradesButton = new JButton("Change Grades");
        changeGradesButton.setEnabled(false);
        logoutButton = new JButton("Logout");
    }
    
    private void setupTextComponents() {
        sNameLabel = new JLabel(sApphndl.getStaff().getForename() + " " + 
                sApphndl.getStaff().getSurname());
        sNameLabel.setFont(new Font("Arial", Font.BOLD, 20));
        sNameLabel.setHorizontalAlignment(JLabel.CENTER);
        sNameLabel.setVerticalAlignment(JLabel.CENTER);

    }
    
    private void setupLists() {
        ArrayList<StaffSubject> subs = sApphndl.getStaff().getStaffSubjects();
        subjectList = new JComboBox(subs.toArray(new Subject[subs.size()]));
        
        studentList = new JList(getStudents());
        studentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        studentList.setLayoutOrientation(JList.VERTICAL);
        studentList.setVisibleRowCount(-1);
        listScroller = new JScrollPane(studentList);
        listScroller.setPreferredSize(new Dimension(250, 150));
        listScroller.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    }
    
    private void setupJPanels() {
        northFormPanel = new JPanel();
        northFormPanel.setBackground(Color.decode("#FFFF99"));
        centerFormPanel = new JPanel();
        centerFormPanel.setBackground(Color.decode("#FFFF99"));
        southFormPanel = new JPanel();
        southFormPanel.setBackground(Color.decode("#FFFF99"));
        
        JPanel subjectListPanel = new JPanel();
        subjectListLabel = new JLabel("Subjects:  ");
        subjectListPanel.add(subjectListLabel);
        subjectListPanel.add(subjectList);
        subjectListPanel.setBackground(Color.decode("#FFFF99"));
        
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(viewGradesButton);
        buttonsPanel.add(changeGradesButton);
        buttonsPanel.setBackground(Color.decode("#FFFF99"));
        
        northFormPanel.add(sNameLabel);
        centerFormPanel.add(subjectListPanel);
        centerFormPanel.add(listScroller);
        centerFormPanel.add(buttonsPanel);
        southFormPanel.add(logoutButton);
        
        getContentPane().add(northFormPanel, BorderLayout.NORTH);
        getContentPane().add(centerFormPanel, BorderLayout.CENTER);
        getContentPane().add(southFormPanel, BorderLayout.SOUTH);
    }
    
    private Student[] getStudents() {
        StaffSubject sub = (StaffSubject)subjectList.getSelectedItem();
        try {
            sub.setStudents(dbcontroller.loadSubjectStudends(sub.getID()));
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return sub.getStudents().toArray(new Student[sub.getStudents().size()]);
    }
    
    private void addActionListeners() {
        subjectList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model = new DefaultListModel();
                for(Student s : getStudents()) {
                    model.addElement(s);
                }
                studentList.setModel(model);
                viewGradesButton.setEnabled(false);
                changeGradesButton.setEnabled(false);
            }
        });
        
        logoutButton.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               int choice = JOptionPane.showConfirmDialog(rootPane, 
                       "Are you sure you want to logout?",
                       "Logout Confirmation", JOptionPane.YES_NO_OPTION, 
                       JOptionPane.QUESTION_MESSAGE);
               
               if(choice == 0) {
                   setVisible(false);
                   dispose();
                   new LoginFrame(dbcontroller);
               }
           } 
        });
        
        studentList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(e.getValueIsAdjusting()) {
                    viewGradesButton.setEnabled(true);
                    changeGradesButton.setEnabled(true);
                }
            }
        }) ;
        
        viewGradesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Student s = (Student)studentList.getSelectedValue();
                StaffSubject staffSubject = (StaffSubject)subjectList.getSelectedItem();
                StudentSubject stuSubject = null;
                try {
                    stuSubject = dbcontroller.getStudentSubject(s.getUsername(), 
                        staffSubject.getID(), staffSubject.getName());
                }
                catch(Exception exp) {
                    System.out.println(exp.getMessage());
                }
                JOptionPane.showMessageDialog(centerFormPanel, stuSubject.toStringGrades(), 
                        staffSubject.getName() + "'s Grades - " + s.getForename() + " " + s.getSurname(), 
                        JOptionPane.INFORMATION_MESSAGE); 
            }
        });
        
        changeGradesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeGradeDialog = new ChangeGradeDialog(null, "Change Grade", 
                        "Change grade for " + subjectList.getSelectedItem() + ":");
                if(changeGradeDialog.wasCancelled()) {
                    StaffSubject sub = (StaffSubject)subjectList.getSelectedItem();
                    Student s = (Student)studentList.getSelectedValue();
                    try {
                        dbcontroller.updateGrade(sub.getID(), s.getUsername(), 
                            changeGradeDialog.getTerm(), changeGradeDialog.getGrade());
                        JOptionPane.showMessageDialog(rootPane, "You have successfully changed the grade.", 
                                "Confirmation Screen", JOptionPane.INFORMATION_MESSAGE);
                    }
                    catch(Exception exp){
                        JOptionPane.showMessageDialog(rootPane, "You have not successfully changed grade.", 
                                "Error Screen", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }            
        });
    }
}
