
package GUI;

import SchoolSystem.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javafx.scene.control.RadioButton;
import javax.swing.*;

public class LoginFrame extends JFrame{
    
    private DatabaseController dbcontroller;
    private JLabel uLabel, pLabel, title, title2;
    private JTextField uField;
    private JPasswordField pField;
    private JButton loginButton;
    private JPanel centerFormPanel, northFormPanel, southFormPanel, eastFormPanel;
    private JRadioButton login1, login2;
    private ButtonGroup group;
    
    public LoginFrame(DatabaseController dbcontroller) {
        super("Hagley Park - Login Screen");
        this.dbcontroller = dbcontroller;
        getContentPane().setLayout(new BorderLayout());
        setupButtons();
        setupTextComponents();
        setupJPanels();
        addActionListeners();
        this.setSize(349,350);
        setLocationRelativeTo(null); 
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void setupButtons() {
        loginButton = new JButton("Login");
        login1 = new JRadioButton("Student",true);
        login1.setBackground(Color.decode("#FFFF99"));
        login2 = new JRadioButton("Staff");
        login2.setBackground(Color.decode("#FFFF99"));
        group = new ButtonGroup();
        group.add(login1);
        group.add(login2);
    }
    
    private void setupTextComponents() {
        title = new JLabel("Welcome to Hagley Park");
        title.setFont(new Font("Arial", Font.BOLD, 20));
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setVerticalAlignment(JLabel.CENTER);
        
        title2 = new JLabel("Login Screen");
        title2.setFont(new Font("Arial", Font.BOLD, 18));
        title2.setHorizontalAlignment(JLabel.CENTER);
        title2.setVerticalAlignment(JLabel.CENTER);
        
        uLabel = new JLabel("Username:");
        
        pLabel = new JLabel("Password:");
        
        uField = new JTextField(15);
        pField = new JPasswordField(15);
    }
    
    private void setupJPanels() { 
        northFormPanel = new JPanel(new FlowLayout());
        northFormPanel.setBackground(Color.decode("#FFFF99"));
        centerFormPanel = new JPanel(new GridLayout(1,1));
        centerFormPanel.setBackground(Color.decode("#FFFF99"));
        southFormPanel = new JPanel(new FlowLayout());
        southFormPanel.setBackground(Color.decode("#FFFF99"));
        
        JPanel titlePanel = new JPanel(new GridLayout(2,1,0,10));
        titlePanel.setBackground(Color.decode("#FFFF99"));
        titlePanel.add(add(title));
        //titlePanel.add(new JLabel(new ImageIcon("school.jpg")));
        titlePanel.add(add(title2));
        northFormPanel.add(titlePanel);
    
        JPanel usernamePanel = new JPanel(new FlowLayout());
        usernamePanel.setBackground(Color.decode("#FFFF99"));
        usernamePanel.add(uLabel);
        usernamePanel.add(uField);
        JPanel passwordPanel = new JPanel(new FlowLayout());
        passwordPanel.setBackground(Color.decode("#FFFF99"));
        passwordPanel.add(pLabel);
        passwordPanel.add(pField);
        
        JPanel loginPanel = new JPanel(new GridLayout(3,1));
        loginPanel.add(usernamePanel);
        loginPanel.add(passwordPanel);
        JPanel loginRadio = new JPanel();
        loginRadio.setBackground(Color.decode("#FFFF99"));
        loginRadio.add(login1);
        loginRadio.add(login2);
        loginPanel.add(loginRadio);
        
        centerFormPanel.add(loginPanel);
        
        southFormPanel.add(new JPanel().add(loginButton));
        
        getContentPane().add(northFormPanel, BorderLayout.NORTH);
        getContentPane().add(centerFormPanel, BorderLayout.CENTER);
        getContentPane().add(southFormPanel, BorderLayout.SOUTH);
        
        getContentPane().add(new JLabel(new ImageIcon("logo.png")), BorderLayout.WEST);
        getContentPane().setBackground(Color.decode("#FFFF99"));
    }
    
    private void addActionListeners() {
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String username = uField.getText();
                String password = pField.getText();
                
                if(login1.isSelected()) {
                    Student student;
                
                    if(username.equals("") || password.equals("")) {
                        JOptionPane.showMessageDialog(centerFormPanel,"You have not "
                            + "enetered your username/password.", "Username/Password error", 
                            JOptionPane.ERROR_MESSAGE); 
                    }
                    else {
                        try {
                        student = dbcontroller.getStudentLogin(username, password);
                        student.setSubjects(dbcontroller.loadStudentSubjects(username));
                        setVisible(false);
                        dispose();
                        new StudentFrame(student, dbcontroller);
                        }
                        catch (Exception exp){
                            JOptionPane.showMessageDialog(centerFormPanel, "Wrong login. Try Again.",
                                    "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
                if(login2.isSelected()) {
                    Staff staff;
                
                    if(username.equals("") || password.equals("")) {
                        JOptionPane.showMessageDialog(centerFormPanel,"You have not "
                            + "enetered your username/password.", "Username/Password error", 
                            JOptionPane.ERROR_MESSAGE); 
                    }
                    else {
                        try {
                        staff = dbcontroller.getStaffLogin(username, password);
                        staff.setStaffSubjects(dbcontroller.loadStaffSubjects(username));
                        setVisible(false);
                        dispose();
                        new StaffFrame(staff, dbcontroller);
                        }
                        catch (Exception exp){
                            JOptionPane.showMessageDialog(centerFormPanel, "Wrong login. Try Again.",
                                    "Error", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
        });
    }
}
