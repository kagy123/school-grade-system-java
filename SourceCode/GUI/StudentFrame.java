package GUI;

import SchoolSystem.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class StudentFrame extends JFrame {
    
    private DatabaseController dbcontroller;
    private StudentAppHandler sApphndl;
    private JButton showGradesButton;
    private JButton showSubjectsButton, logoutButton;
    private JLabel sNameLabel;
    private JTextArea textArea;
    private JPanel centerFormPanel, northFormPanel, southFormPanel, eastFormPanel;
    private JScrollPane scrollPane;
    private JRadioButton termRadio1, termRadio2, termRadio3, termRadio4;
    private ButtonGroup group;
    
    public StudentFrame(Student student, DatabaseController dbcontroller) {
        super("School System - Student Menu");
        this.dbcontroller = dbcontroller;
        sApphndl = new StudentAppHandler(student);
        getContentPane().setLayout(new BorderLayout());
        setupButtons();
        setupTextComponents();
        setupJPanels();
        addActionListeners(); 
        this.setSize(490,350);
        setLocationRelativeTo(null); 
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void setupButtons() {
        showGradesButton = new JButton("Show Grades");
        showSubjectsButton = new JButton("Show Subjects");
        logoutButton = new JButton("Logout");
        termRadio1 = new JRadioButton("First term",true);
        termRadio1.setBackground(Color.decode("#FFFF99"));
        termRadio2 = new JRadioButton("Second term");
        termRadio2.setBackground(Color.decode("#FFFF99"));
        termRadio3 = new JRadioButton("Third term");
        termRadio3.setBackground(Color.decode("#FFFF99"));
        termRadio4 = new JRadioButton("Overall grades");
        termRadio4.setBackground(Color.decode("#FFFF99"));
        group = new ButtonGroup();
        group.add(termRadio1);
        group.add(termRadio2);
        group.add(termRadio3);
        group.add(termRadio4);
    }
    
    private void setupTextComponents() {
        sNameLabel = new JLabel(sApphndl.getStudent().getForename() 
                + " " + sApphndl.getStudent().getSurname());
        sNameLabel.setFont(new Font("Arial", Font.BOLD, 20));
        sNameLabel.setHorizontalAlignment(JLabel.CENTER);
        sNameLabel.setVerticalAlignment(JLabel.CENTER);
        
        textArea = new JTextArea(10,20);
        scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        textArea.setEditable(false);
    }
    
    private void setupJPanels() {
        northFormPanel = new JPanel(new GridLayout(2,1));
        northFormPanel.setBackground(Color.decode("#FFFF99"));
        centerFormPanel = new JPanel();
        centerFormPanel.setBackground(Color.decode("#FFFF99"));
        southFormPanel = new JPanel();
        southFormPanel.setBackground(Color.decode("#FFFF99"));
        eastFormPanel = new JPanel(new GridLayout(3,1));
        eastFormPanel.setBackground(Color.decode("#FFFF99"));
        
        JPanel[] p = new JPanel[2];
        for(int i = 0; i < p.length; i++) {
            p[i] = new JPanel();
        }
        
        p[0].add(sNameLabel);
        p[0].setBackground(Color.decode("#FFFF99"));
        p[1].setLayout(new FlowLayout());
        p[1].add(showSubjectsButton);
        p[1].add(showGradesButton);
        p[1].setBackground(Color.decode("#FFFF99"));
        northFormPanel.add(p[0]);
        northFormPanel.add(p[1]);
        
        centerFormPanel.add(scrollPane);
        southFormPanel.add(logoutButton);
        eastFormPanel.add(termRadio1);
        eastFormPanel.add(termRadio2);
        eastFormPanel.add(termRadio3);
        eastFormPanel.add(termRadio4);
        
        
        getContentPane().add(northFormPanel, BorderLayout.NORTH);
        getContentPane().add(centerFormPanel, BorderLayout.CENTER);
        getContentPane().add(southFormPanel, BorderLayout.SOUTH);
        getContentPane().add(eastFormPanel, BorderLayout.EAST);
        
    }
    
    private void addActionListeners() {
        showGradesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                textArea.setText(sApphndl.getAllStudentGrades());
//                textArea.setCaretPosition(0);
                if(termRadio1.isSelected()) {
                    textArea.setText(sApphndl.getFirstTermGrades());
                    textArea.setCaretPosition(0);
                }
                else if(termRadio2.isSelected()) {
                    textArea.setText(sApphndl.getSecondTermGrades());
                    textArea.setCaretPosition(0);
                }
                else if(termRadio3.isSelected()) {
                    textArea.setText(sApphndl.getThirdTermGrades());
                    textArea.setCaretPosition(0);
                }
                else if(termRadio4.isSelected()) {
                    textArea.setText(sApphndl.getOverallGrades());
                    textArea.setCaretPosition(0);
                }
                else {
                    JOptionPane.showMessageDialog(centerFormPanel,"You have not "
                        + "selected term.", "Grades error", 
                        JOptionPane.ERROR_MESSAGE); 
                }
            }
        });
        
        showSubjectsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText(sApphndl.getStudentSubjects());
                textArea.setCaretPosition(0);
            }
        });
        
        logoutButton.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               int choice = JOptionPane.showConfirmDialog(rootPane, 
                       "Are you sure you want to logout?",
                       "Logout Confirmation", JOptionPane.YES_NO_OPTION, 
                       JOptionPane.QUESTION_MESSAGE);
               if(choice == 0) {
                   setVisible(false);
                   dispose();
                   new LoginFrame(dbcontroller);
               }
           } 
        });
    }
}
